package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (Objects.equals(x, null) || Objects.equals(y, null)) {
            throw new IllegalArgumentException();
        }
        Iterator iterX = x.iterator();
        Iterator iterY = y.iterator();
        while (true) {
            if (!iterX.hasNext()) {
                return true;
            }
            Object a = iterX.next();
            while (true) {
                if (!iterY.hasNext()) {
                    return false;
                }
                if (Objects.equals(iterY.next(), a)) {
                    break;
                }
            }
        }
    }
}
