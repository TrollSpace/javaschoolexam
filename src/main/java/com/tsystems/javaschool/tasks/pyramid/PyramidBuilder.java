package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int[][] table;
        int size = inputNumbers.size();
        int count = 0;
        int rows = 1;
        int cols = 1;
        while (count < size) {
            count = count + rows;
            rows++;
            cols = cols + 2;
        }
        rows = rows - 1;
        cols = cols - 2;

        if (size == count) {
            try {
                Collections.sort(inputNumbers);
            } catch (Error e) {
                throw new CannotBuildPyramidException();
            }
            table = new int[rows][cols];

            int center = (cols / 2);
            count = 1;
            int arrIdx = 0;

            for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
                int start = center - offset;
                for (int j = 0; j < count * 2; j += 2, arrIdx++) {
                    table[i][start + j] = inputNumbers.get(arrIdx);
                }
            }

            for (int[] a : table) {
                for (int b : a)
                    System.out.print(b + " ");
                System.out.println();
            }
        } else {
            throw new CannotBuildPyramidException();
        }
        return table;
    }

}
