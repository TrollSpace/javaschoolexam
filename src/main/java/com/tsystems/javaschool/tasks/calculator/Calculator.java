package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Stack;

public class Calculator {
//    public static final Map<String, Integer> INDEX_OPERATIONS;
//
//    static {
//        INDEX_OPERATIONS = new HashMap<>();
//        INDEX_OPERATIONS.put("*", 1);
//        INDEX_OPERATIONS.put("/", 1);
//        INDEX_OPERATIONS.put("+", 2);
//        INDEX_OPERATIONS.put("-", 2);
//    }
//
//    public static String sortingStation(String statement, Map<String, Integer> operations, String leftBrk,
//                                        String rightBrk) {
//        List<String> out = new ArrayList<>();
//        Stack<String> stack = new Stack<>();
//
//        Set<String> operationSymbols = new HashSet<>(operations.keySet());
//        operationSymbols.add(leftBrk);
//        operationSymbols.add(rightBrk);
//
//        int index = 0;
//        boolean hasNext = true;
//        StringBuilder result = new StringBuilder();
//        while (hasNext) {
//            int nextOperationIndex = statement.length();
//            String nextOperation = "";
//            for (String operation : operationSymbols) {
//                int i = statement.indexOf(operation, index);
//                if (i >= 0 && i < nextOperationIndex) {
//                    nextOperation = operation;
//                    nextOperationIndex = i;
//                }
//            }
//            if (nextOperationIndex == statement.length()) {
//                hasNext = false;
//            } else {
//                if (index != nextOperationIndex) {
//                    out.add(statement.substring(index, nextOperationIndex));
//                }
//                if (nextOperation.equals(leftBrk)) {
//                    stack.push(nextOperation);
//                } else if (nextOperation.equals(rightBrk)) {
//                    while (!stack.peek().equals(leftBrk)) {
//                        out.add(stack.pop());
//                        if (stack.empty()) {
//                            return result.toString();
//                        }
//                    }
//                    stack.pop();
//                } else {
//                    while (!stack.empty() && !stack.peek().equals(leftBrk) &&
//                            (operations.get(nextOperation) >= operations.get(stack.peek()))) {
//                        out.add(stack.pop());
//                    }
//                    stack.push(nextOperation);
//                }
//                index = nextOperationIndex + nextOperation.length();
//            }
//        }
//        if (index != statement.length()) {
//            out.add(statement.substring(index));
//        }
//        while (!stack.empty()) {
//            out.add(stack.pop());
//        }
//        if (!out.isEmpty())
//            result.append(out.remove(0));
//        while (!out.isEmpty())
//            result.append(" ").append(out.remove(0));
//
//        return result.toString();
//    }
//
//    public static String sortingStation(String statement, Map<String, Integer> operations) {
//        return sortingStation(statement, operations, "(", ")");
//    }
//
//    /**
//     * Evaluate statement represented as string.
//     *
//     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
//     *                  parentheses, operations signs '+', '-', '*', '/'<br>
//     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
//     * @return string value containing result of evaluation or null if statement is invalid
//     */
//    public static String evaluate(String statement) {
//        if(statement.isEmpty()){
//            return null;
//        }
//        statement = statement.replace(",", ".");
//        String rpn = sortingStation(statement, INDEX_OPERATIONS, "(", ")");
//        StringTokenizer tokenizer = new StringTokenizer(rpn, " ");
//        Stack<Double> stack = new Stack<>();
//        while (tokenizer.hasMoreTokens()) {
//            String token = tokenizer.nextToken();
//            if (!INDEX_OPERATIONS.keySet().contains(token)) {
//                stack.push(new Double(token));
//            } else {
//                double operand2 = stack.pop();
//                double operand1 = stack.empty() ? 0 : stack.pop();
//                switch (token) {
//                    case "*":
//                        stack.push(operand1 * operand2);
//                        break;
//                    case "/":
//                        stack.push(operand1 / operand2);
//                        break;
//                    case "+":
//                        stack.push(operand1 + operand2);
//                        break;
//                    case "-":
//                        stack.push(operand1 - operand2);
//                        break;
//                }
//            }
//        }
//        if (stack.size() != 1)
//            return null;
//        return stack.pop().toString();
//
//    }
    public static String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        if (statement.indexOf(',') != -1 || !checkStatement(statement) || statement.isEmpty()) {
            return null;
        }
        try {
            String s = (new ScriptEngineManager().getEngineByName("JavaScript").eval(statement)).toString();
            if (s.equals("Infinity")) {
                return null;
            } else return s;
        } catch (ScriptException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean checkStatement(String statement) {
        Stack<Character> stack = new Stack<>();
        if (statement.contains("//") || statement.contains("++") || statement.contains("--") || statement.contains("**") || statement.contains("..")) {
            return false;
        }
        for (char c : statement.toCharArray()) {
            if (c == ')' && stack.isEmpty()) {
                return false;
            }
            if (c == '(') {
                stack.push(c);
            }
            if (c == ')') {
                if (stack.peek() == '(') {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
